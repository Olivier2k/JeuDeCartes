﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace JeuDeCartes
{
    public class Carte
    {
        public string Nom
        {
            get
            {
                switch (_valeur)
                {
                    case 1:
                        {
                            return string.Format("As de {0}", RetourneTypeCarteTexte(_typeCarte));
                        }
                    case 11:
                        {
                            return string.Format("Valet de {0}", RetourneTypeCarteTexte(_typeCarte));
                        }
                    case 12:
                        {
                            return string.Format("Dame de {0}", RetourneTypeCarteTexte(_typeCarte));
                        }
                    case 13:
                        {
                            return string.Format("Roi de {0}", RetourneTypeCarteTexte(_typeCarte));
                        }
                    default:
                        {
                            return string.Format("{0} de {1}", _valeur.ToString(), RetourneTypeCarteTexte(_typeCarte));
                        }

                }
            }

        }

        private int _valeur;
        public int Valeur
        {
            get { return _valeur; }
            set { _valeur = value; }
        }

        private TypesCartes _typeCarte;
        public TypesCartes TypeCarte
        {
            get { return _typeCarte; }
            set { _typeCarte = value; }
        }

        private Image _image;
        public Image Image
        {
            get { return _image; }
            set { _image = value; }
        }


        public enum TypesCartes
        {
            Coeur = 0,
            Carreau = 1,
            Piques = 2,
            Trefle = 3
        }

        public string RetourneTypeCarteTexte(TypesCartes typeCarte)
        {
            switch (typeCarte)
            {
                case TypesCartes.Coeur: { return "coeur"; }
                case TypesCartes.Carreau: { return "carreau"; }
                case TypesCartes.Piques: { return "piques"; }
                case TypesCartes.Trefle: { return "trèfle"; }
                default: { return null; }
            }
        }

    }
}
